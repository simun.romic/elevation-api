# Project elevation-api

## Requirements

- `Node v12`

# Deployment

- `npm i` - to install required dependencies
- `npm run deploy:${stage}` - to deploy lambda func and create API GW for provided `${stage}` value.
- `npm run remove:${stage}` - to remove lambda func and API GW for provided `${stage}` value.

- possible `${stage}` values are:
  - dev
  - qa
  - prod
