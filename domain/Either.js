export default class Either {
  constructor(error, data, contextData) {
    this.p_error = error;
    this.p_data = data;
    this.p_contextData = contextData;
  }

  get error() {
    return this.p_error;
  }

  get data() {
    return this.p_data;
  }

  get contextData() {
    return this.p_contextData;
  }

  isError() {
    return !!this.p_error;
  }

  isSuccess() {
    return !!this.p_data;
  }
}
