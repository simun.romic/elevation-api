import { STATUS_CODE } from '../utils/constants';

/**
 *
 * @param {Error} error
 * @returns {Object}
 */
const handleError = (error) => {
  let code = STATUS_CODE.INTERNAL_SERVER_ERROR;
  const errorMsg = {};

  if (error.response) {
    /*
     * The request was made and the server responded with a
     * status code that falls out of the range of 2xx
     */
    code = error.response.status;
    errorMsg.msg = error.response.data;
  } else if (error.request) {
    /*
     * The request was made but no response was received, `error.request`
     * is an instance of XMLHttpRequest in the browser and an instance
     * of http.ClientRequest in Node.js
     */
    errorMsg.msg = error.toJSON().message;
  } else {
    // Something happened in setting up the request and triggered an Error
    errorMsg.msg = error.message;
  }

  return {
    code,
    errorMsg,
  };
};

export default handleError;
