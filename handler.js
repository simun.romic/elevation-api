/* eslint-disable import/prefer-default-export */

import os from 'os';

import ElevationService from './services/ElevationService';

import generateResponse from './utils/responseUtils';

import { STATUS_CODE, CONTENT_TYPE } from './utils/constants';

import handleError from './handlers/errorHandler';

const { HGT_BUCKET } = process.env;

const workdir = os.tmpdir();

const elevationService = new ElevationService(workdir, HGT_BUCKET);

/**
 *
 * @param {Object} event
 * @return {Promise}
 */
const handle = async (event) => {
  try {
    const { body } = event;

    const coordinates = JSON.parse(body);

    const elevations = await elevationService.calculateElevations(coordinates);

    const statusCode = STATUS_CODE.OK;
    const responseHeaders = {
      'Content-Type': CONTENT_TYPE.APPLICATION_JSON,
    };

    return generateResponse(statusCode, responseHeaders, elevations);
  } catch (error) {
    const { code, errorMsg } = handleError(error);
    console.log(`Error while processing elevation request: ${errorMsg}: ${event.body}`);

    return {
      statusCode: code,
      headers: {
        'Content-Type': CONTENT_TYPE.APPLICATION_JSON,
      },
      body: JSON.stringify(errorMsg),
    };
  }
};

export { handle };
