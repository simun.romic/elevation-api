/**
 *
 * @param {Array<Number>} coordinate
 * @returns {String}
 */
const fileNameGenerator = (coordinate) => {
  const [lon, lat] = coordinate;

  let latChar;
  let lonChar;

  if (lat > 0) {
    latChar = 'N';
  } else {
    latChar = 'S';
  }

  if (lon > 0) {
    lonChar = 'E';
  } else {
    lonChar = 'W';
  }

  const hgtFileName = `${latChar}${Math.abs(lat)}${lonChar}${`00${Math.abs(lon)}`.slice(-3)}.hgt`;

  return hgtFileName;
};

export default fileNameGenerator;
