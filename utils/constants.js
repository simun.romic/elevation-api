const CONTENT_TYPE = Object.freeze({
  APPLICATION_JSON: 'application/json',
  PLAIN_TEXT: 'text/plain',
});

const STATUS_CODE = Object.freeze({
  OK: 200,
  INTERNAL_SERVER_ERROR: 500,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
});

const HTTP_REQUEST_METHOD = Object.freeze({
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  PATCH: 'PATCH',
  DELETE: 'DELETE',
  HEAD: 'HEAD',
  OPTIONS: 'OPTIONS',
});

const S3_ACL = Object.freeze({
  PRIVATE: 'private',
  PUBLIC_READ: 'public-read',
  PUBLIC_READ_WRITE: 'public-read-write',
});

export { CONTENT_TYPE, STATUS_CODE, HTTP_REQUEST_METHOD, S3_ACL };
