import { constants, promises } from 'fs';

import { safeExecutionEither } from './promiseUtils';

/**
 *
 * @param {Array<String>} files
 * @returns {Promise<Array<String>>}
 */
const getExistingFiles = async (files) => {
  const checkFilesPromises = files.map(async (filePath) =>
    safeExecutionEither(promises.access(filePath, constants.F_OK), { fileName: filePath }),
  );

  const checkFilesResults = await Promise.all(checkFilesPromises);

  const foundFiles = checkFilesResults
    .filter((res) => res.isSuccess())
    .map((res) => res.contextData.fileName);

  return foundFiles;
};

/**
 *
 * @param {Array<String>} files
 * @returns {Promise<Array<String>>}
 */
const getNonExistingFiles = async (files) => {
  const checkFilesPromises = files.map(async (filePath) =>
    safeExecutionEither(promises.access(filePath, constants.F_OK), { fileName: filePath }),
  );

  const checkFilesResults = await Promise.all(checkFilesPromises);

  const notFoundFiles = checkFilesResults
    .filter((res) => res.isError())
    .map((res) => res.contextData.fileName);

  return notFoundFiles;
};

export { getExistingFiles, getNonExistingFiles };
