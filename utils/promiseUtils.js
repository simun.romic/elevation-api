import Either from '../domain/Either';

/**
 * Wrap Promise to safe Promise
 * Catch exeception and return error instance instead of throwing it
 * @param {Promise} p
 * @param {Boolean} showErrorLog
 * @returns {Promise}
 */
const safeExecution = async (p, showErrorLog = false) => {
  try {
    return await p;
  } catch (error) {
    if (showErrorLog) {
      console.error(`Error while executing promise with error: ${error}`);
    }
    return error;
  }
};

/**
 * Wrap Promise to safe Promise
 * Catch exeception and return error instance instead of throwing it
 * @param {Promise} p
 * @param {Object} defaultValue
 * @param {Boolean} showErrorLog
 * @returns {Promise}
 */
const safeExecutionWithDefault = async (p, defaultValue, showErrorLog = false) => {
  try {
    return await p;
  } catch (error) {
    if (showErrorLog) {
      console.error(`Error while executing promise with error: ${error}`);
    }
    return defaultValue;
  }
};

/**
 * Wrap Promise to safe Promise by catching exeception
 * return `Either` instance
 * @param {Promise} p
 * @param {Object} contextData
 * @param {Boolean} showErrorLog
 * @returns {Promise<Either>}
 */
const safeExecutionEither = async (p, contextData, showErrorLog = false) => {
  try {
    const data = await p;
    return new Either(null, data, contextData);
  } catch (error) {
    if (showErrorLog) {
      console.error(`Error while executing promise with error: ${error}`);
    }
    return new Either(error, null, contextData);
  }
};

export { safeExecution, safeExecutionWithDefault, safeExecutionEither };
