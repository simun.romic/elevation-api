import { join, basename } from 'path';

import { TileSet } from 'node-hgt';

import S3Service from './S3Service';

import { safeExecutionEither, safeExecutionWithDefault } from '../utils/promiseUtils';
import fileNameGenerator from '../utils/hgtUtils';
import { CONTENT_TYPE, S3_ACL } from '../utils/constants';
import { getNonExistingFiles } from '../utils/fsUtils';

// TODO batching coordinates to smaller batches???
// TODO split up this processing part to multiple lambdas instead of running in one??? -- to speed up process
// TODO SQS with lambdas for downloading and uploading missed hgt files to s3 in order to save processing time from this lambda?
class ElevationService {
  /**
   *
   * @param {String} dataFolder
   * @param {String} bucket
   */
  constructor(dataFolder, bucket) {
    this.s3Service = new S3Service();
    this.tileSet = new TileSet(dataFolder);
    this.dataFolder = dataFolder;
    this.hgtFilesBucket = bucket;
  }

  /**
   *
   * @param {Object}
   * @returns {Promise<Array<Number>>}
   */
  async getElevation({ lat, lon }) {
    return new Promise((resolve, reject) => {
      this.tileSet.getElevation([lat, lon], (err, elevation) => {
        if (err) {
          console.log(`getElevation failed ${[lat, lon]}: ${err.message}`);
          reject(err);
        } else {
          const roundedElevation = Number(elevation.toFixed(1));
          const coordinate = [lon, lat, roundedElevation];
          resolve(coordinate);
        }
      });
    });
  }

  /**
   *
   * @param {Array<Array<Number>>} coordinates
   * @returns {Promise<Array<Array<Number>>}
   */
  async getElevations(coordinates) {
    const elevationRequests = coordinates.map((coordinate) => {
      const [lon, lat] = coordinate;
      const defaultCoordinate = [lon, lat, null];
      return safeExecutionWithDefault(this.getElevation({ lat, lon }), defaultCoordinate);
    });

    const elevations = await Promise.all(elevationRequests);

    return elevations;
  }

  /**
   *
   * @param {Array<Array<Number>>} coordinates
   * @returns {Promise<Array<Array<Number>>}
   */
  async calculateElevations(coordinates) {
    const commonCoordinates = [];

    coordinates.forEach((coordinate) => {
      const idx = commonCoordinates.findIndex((e) =>
        coordinate.every((v, i) => Math.floor(v) === Math.floor(e[i])),
      );
      if (idx === -1) {
        const roundedCoordinate = coordinate.map((c) => Math.floor(c));
        commonCoordinates.push(roundedCoordinate);
      }
    });

    const hgtFileNames = commonCoordinates.map((commonCoordinate) => {
      const hgtFileName = fileNameGenerator(commonCoordinate);
      return join(this.dataFolder, hgtFileName);
    });

    // check if some hgt files are already downloaded locally
    const missingHgtFiles = await getNonExistingFiles(hgtFileNames);
    console.log(`missingHgtFiles: ${missingHgtFiles}`);

    const hgtFilesDownloadRequests = missingHgtFiles.map((hgtFile) =>
      safeExecutionEither(
        this.s3Service.downloadFile(this.hgtFilesBucket, basename(hgtFile), hgtFile),
        {
          fileName: hgtFile,
        },
      ),
    );

    const hfgFilesDownloadResponse = await Promise.all(hgtFilesDownloadRequests);

    const elevations = await this.getElevations(coordinates);

    const missedHgtFiles = hfgFilesDownloadResponse
      .filter((response) => response.isError())
      .map((response) => response.p_contextData.fileName);

    console.log(`missedHgtFiles: ${JSON.stringify(missedHgtFiles)}`);

    // upload missing HGT files to S3
    const uploadFilesPromises = missedHgtFiles.map((file) =>
      safeExecutionEither(
        this.s3Service.uploadFile(
          file,
          this.hgtFilesBucket,
          basename(file),
          CONTENT_TYPE.PLAIN_TEXT,
          S3_ACL.PRIVATE,
        ),
      ),
    );
    await Promise.all(uploadFilesPromises);

    return elevations;
  }
}

export default ElevationService;
