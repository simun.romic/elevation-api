// eslint-disable-next-line import/no-extraneous-dependencies
import S3 from 'aws-sdk/clients/s3';

import fs from 'fs';

class S3Service {
  constructor() {
    this.s3Client = new S3();
    this.fs = fs;
  }

  async downloadFile(bucket, fileKey, filePath) {
    return new Promise((resolve, reject) => {
      const fileWriteStream = this.fs.createWriteStream(filePath);

      const fileReadStream = this.s3Client
        .getObject({
          Bucket: bucket,
          Key: fileKey,
        })
        .createReadStream();

      fileReadStream.on('error', async (err) => {
        await this.fs.promises.unlink(filePath);

        reject(err);
      });

      fileWriteStream.on('error', async (err) => {
        await this.fs.promises.unlink(filePath);

        reject(err);
      });

      fileWriteStream.on('finish', () => {
        resolve(filePath);
      });

      fileReadStream.pipe(fileWriteStream);
    });
  }

  async uploadFile(filePath, bucket, key, contentType, acl) {
    return this.s3Client
      .upload({
        Bucket: bucket,
        Key: key,
        Body: fs.createReadStream(filePath),
        ACL: acl,
        ContentType: contentType,
      })
      .promise();
  }

  async fileExists(bucket, fileKey) {
    try {
      const params = {
        Bucket: bucket,
        Key: fileKey,
      };
      await this.s3Client.headObject(params).promise();
      return true;
    } catch (err) {
      return false;
    }
  }
}

export default S3Service;
